import axios from 'axios'

const BASE_URL = "https://projeto-backend-v2.herokuapp.com"

export default class ApiHelper {

  static async getPostos() {
    const url = `${BASE_URL}/api/postos/`
    const response = await axios.get(url)
    return response.data
  }

  static async addCombustivel(novoPreco, postoId) {
    const url = `${BASE_URL}/api/combustivel/${postoId}`
    const result = await axios.post(url, novoPreco, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json"
      },
    })
    return result
  }

  static async getPostoById(id) {
    const url = `${BASE_URL}/api/postos/${id}`

    const response = await axios.get(url)
    return response.data
  }
}