export function maskCombustivel(nome) {
  switch (nome) {
    case "diesel":
      return "Diesel";
    case "etanol":
      return "Etanol";
    case "etanol_aditivado":
      return "Álcool Aditivado";
    case "gasolina":
      return "Gasolina";
    case "gasolina_aditivada":
      return "Gasolina Aditivada";
    case "gasolina_premium":
      return "Gasolina Premium";
    case "gasolina_formulada":
      return "Gasolina Formulada";
    case "gnv":
      return "GNV";
    default:
      return "Combustível";
  }
}

export function maskPreco(preco) {
  return (preco + "").replace(".", ",");
}
