import { createStackNavigator } from "react-navigation";

import HomeScreen from "./components/HomeScreen";
import PostoScreen from "./components/PostoScreen";
import ListScreen from "./components/ListScreen";
import AddCombustivelScreen from "./components/AddCombustivelScreen";

const App = createStackNavigator({
  Home: HomeScreen,
  Posto: PostoScreen,
  List: ListScreen,
  AddCombustivel: AddCombustivelScreen
});

export default App;
