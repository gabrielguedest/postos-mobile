import React, { Component } from "react";

import { View, TextInput } from "react-native";

import ModalSelector from "react-native-modal-selector";

import { maskCombustivel } from "../utils/masks";
import { StatusBar } from "react-native";

export default class CombustivelSelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      opcoes: [],
      textInputValue: ""
    };
  }

  getCombustiveis(combustiveis) {
    let combs = [];
    let i = 0;

    for (let key in combustiveis) {
      if (combustiveis[key][0]) {
        combs.push({
          key: i,
          label: maskCombustivel(key),
          combustivel: combustiveis[key]
        });
      }

      i++;
    }

    return combs;
  }

  async componentDidMount() {
    const opcoes = await this.getCombustiveis(this.props.combustiveis);
    this.setState({ opcoes });
  }

  render() {
    return (
      <ModalSelector
        data={this.state.opcoes}
        initValue="Selecione um combustível"
        cancelText="Cancelar"
        onChange={option => {
          this.setState({ textInputValue: option.label });
          this.props.selectComb(option.combustivel);
        }}
      >
        <TextInput
          style={{
            borderWidth: 1,
            borderColor: "#ccc",
            padding: 10,
            color: "#fff",
            textAlign: "center"
          }}
          editable={false}
          placeholder="Selecione um combustível"
          value={this.state.textInputValue}
        />
      </ModalSelector>
    );
  }
}
