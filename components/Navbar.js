import React, { Component } from "react";

import {
  Alert,
  StyleSheet,
  View,
  Image,
  TouchableHighlight
} from "react-native";

export default class Navbar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.navbar}>
        <TouchableHighlight
          style={styles.menuButton}
          onPress={() =>
            Alert.alert("Ops!", "Funcionalidade não adicionada.")
          }
        >
          <Image
            source={require("../assets/hamburger-icon.png")}
            style={{ width: 30, height: 30 }}
          />
        </TouchableHighlight>

        <TouchableHighlight
          style={styles.listButton}
          onPress={this.props.navigateListScreen()}
        >
          <Image
            source={require("../assets/list-icon.png")}
            style={{ width: 30, height: 30 }}
          />
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navbar: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    height: 200
  },
  menuButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#35386d",
    height: 45,
    width: 45,
    borderRadius: 35
  },
  listButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderColor: "#fff",
    height: 45,
    width: 45,
    borderRadius: 35,
    backgroundColor: "rgba(0,0,0,0.1)"
  }
});
