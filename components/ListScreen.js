import React, { Component } from "react";

import {
  View,
  StyleSheet,
  Text,
  StatusBar,
  TouchableHighlight
} from "react-native";

import { Header, Icon } from "react-native-elements";

import CombustivelSelect from "./CombustivelSelect";
import ListCombustiveis from "./ListCombustiveis";
import ListPostos from "./ListPostos";

export default class ListScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tabOption: "combustivel"
    };

    this.renderList = this.renderList.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.setTabOption = this.setTabOption.bind(this);
  }

  static navigationOptions = {
    title: "Lista Postos/Combustiveis",
    header: null
  };

  handleClick(posto_id) {
    this.props.navigation.push("Posto", { posto_id });
  }

  renderList(postos) {
    if (this.state.tabOption === "combustivel") {
      return (
        <ListCombustiveis postos={postos} handleClick={this.handleClick} />
      );
    } else {
      return <ListPostos postos={postos} handleClick={this.handleClick} />;
    }
  }

  setTabOption(option) {
    this.setState({ tabOption: option });
  }

  render() {
    const { navigation } = this.props;
    const { tabOption } = this.state;
    const postos = navigation.getParam("postos");

    return (
      <View
        style={{ position: "absolute", bottom: 0, top: 0, left: 0, right: 0 }}
      >
        <Header
          backgroundColor="#30336b"
          outerContainerStyles={{ borderBottomWidth: 0 }}
          leftComponent={{
            icon: "arrow-back",
            color: "#fff",
            underlayColor: "#30336b",
            onPress: () => this.props.navigation.goBack()
          }}
          centerComponent={{
            text: "Lista",
            style: {
              color: "#fff",
              fontSize: 18,
              fontWeight: "bold"
            }
          }}
        />
        <View
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            height: 50
          }}
        >
          <TouchableHighlight
            style={tabOption === "combustivel" ? styles.active : styles.tab}
            onPress={() => this.setTabOption("combustivel")}
          >
            <Text
              style={
                tabOption === "combustivel" ? styles.textActive : styles.text
              }
            >
              Combustíveis
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={tabOption === "posto" ? styles.active : styles.tab}
            onPress={() => this.setTabOption("posto")}
          >
            <Text
              style={tabOption === "posto" ? styles.textActive : styles.text}
            >
              Postos
            </Text>
          </TouchableHighlight>
        </View>
        {this.renderList(postos)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tab: {
    width: "50%",
    backgroundColor: "#30336b",
    display: "flex",
    justifyContent: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#30336b"
  },
  active: {
    width: "50%",
    backgroundColor: "#30336b",
    display: "flex",
    justifyContent: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#ff1744"
  },
  text: {
    fontWeight: "bold",
    color: "#bdbdbd",
    textAlign: "center"
  },
  textActive: {
    fontWeight: "bold",
    color: "white",
    textAlign: "center"
  }
});
