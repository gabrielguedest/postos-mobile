import React, { Component } from "react";

import { StyleSheet, ScrollView, Text, View } from "react-native";

import { LinearGradient } from "expo";

import { maskCombustivel } from "../utils/masks";

export default class CombustivelDetail extends Component {
  getCombustivel() {
    const retorno = [];

    if (this.props.postoSelecionado) {
      let { combustiveis } = this.props.postoSelecionado;

      for (let key in combustiveis) {
        if (combustiveis[key][0]) {
          retorno.push(
            <View style={styles.details} key={combustiveis[key][0].preco}>
              <Text
                style={{
                  fontFamily: "Roboto",
                  fontSize: 56,
                  fontWeight: "bold",
                  color: "#30336b",
                  marginBottom: 3
                }}
              >
                {`${combustiveis[key][0].preco}`.replace('.', ',')}
              </Text>
              <Text
                style={{
                  fontFamily: "Roboto",
                  fontSize: 14,
                  fontWeight: "100",
                  color: "#2e2e2e"
                }}
              >
                {maskCombustivel(key)}
              </Text>
            </View>
          );
        }
      }
    }

    return retorno;
  }

  render() {
    const { postoSelecionado } = this.props;

    return (
      <View
        style={{
          position: "absolute",
          bottom: 0,
          width: "100%",
          minHeight: 300
        }}
      >
        <ScrollView
          style={styles.detailsContainer}
          horizontal
          showsHorizontalScrollIndicator={false}
        >
          {this.getCombustivel()}
        </ScrollView>

        <Text
          style={{
            zIndex: 1,
            color: "#fff",
            fontSize: 22,
            fontFamily: "Roboto",
            fontWeight: "bold",
            textAlign: "center",
            paddingTop: 10
          }}
        >
          {postoSelecionado.nome}
        </Text>

        <Text
          style={{
            zIndex: 1,
            color: "#dddddd",
            fontSize: 14,
            fontFamily: "Roboto",
            textAlign: "center"
          }}
        >
          {postoSelecionado.localizacao.endereco} -{" "}
          {postoSelecionado.localizacao.numero}
        </Text>

        <Text
          style={{
            zIndex: 1,
            color: "#fff",
            fontSize: 14,
            fontFamily: "Roboto",
            textAlign: "center",
            paddingTop: 15,
            paddingBottom: 20,
            textDecorationLine: "underline"
          }}
          onPress={() => this.props.navigatePostoScreen(postoSelecionado._id)}
        >
          Clique para detalhes
        </Text>

        <LinearGradient
          colors={["transparent", "rgba(0,0,0,0.4)", "rgba(0,0,0,0.8)"]}
          style={{
            position: "absolute",
            bottom: 0,
            width: "100%",
            minHeight: 500
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  detailsContainer: {
    display: "flex",
    zIndex: 1,
    width: "100%",
    maxHeight: 300
  },
  details: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 200,
    height: 150,
    backgroundColor: "#fff",
    marginLeft: 20,
    marginRight: 20,
    marginVertical: 10,
    borderRadius: 15
  }
});
