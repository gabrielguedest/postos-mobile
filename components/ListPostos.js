import React, { Component } from "react";

import { ScrollView, View, Text } from "react-native";
import { ListItem } from "react-native-elements";

const ListPostos = props => (
  <ScrollView>
    {props.postos.map(posto => (
      <ListItem
        key={posto._id}
        title={posto.nome}
        titleStyle={{
          fontWeight: "bold",
          fontSize: 16,
          color: "black",
          opacity: 0.8
        }}
        subtitle={
          <View>
            <Text style={{ opacity: 0.7 }}>
              {posto.localizacao.endereco}, {posto.localizacao.numero}
            </Text>
            <Text style={{ opacity: 0.6 }}>Bandeira {posto.bandeira}</Text>
          </View>
        }
        leftIcon={{ name: "local-gas-station", size: 40, color: "#30336b" }}
        onPress={() => props.handleClick(posto._id)}
      />
    ))}
  </ScrollView>
);

export default ListPostos;
