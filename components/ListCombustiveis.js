import React, { Component } from "react";

import { View, Text, ScrollView } from "react-native";

import { ListItem } from "react-native-elements";

import { maskPreco } from "../utils/masks";

import CombustivelSelectList from "./CombustivelSelectList";

export default class ListCombustiveis extends Component {
  constructor(props) {
    super(props);

    this.state = {
      combustivelSelecionado: "gasolina",
      combustiveis: []
    };

    this.getCombustiveis = this.getCombustiveis.bind(this);
    this.setCombustivel = this.setCombustivel.bind(this);
  }

  async componentDidMount() {
    this.setCombustivel();
  }

  getCombustiveis(postos) {
    const { combustivelSelecionado } = this.state;

    const combustiveisOrdenados = [];

    postos.map(posto => {
      if (posto.combustiveis[combustivelSelecionado][0]) {
        let combustivel = {
          ...posto.combustiveis[combustivelSelecionado][0],
          posto: {
            id: posto._id,
            nome: posto.nome,
            bandeira: posto.bandeira
          }
        };

        combustiveisOrdenados.push(combustivel);
      }
    });

    combustiveisOrdenados.sort((a, b) => {
      if (a.preco > b.preco) return 1;
      if (a.preco < b.preco) return -1;
      return 0;
    });

    return combustiveisOrdenados;
  }

  async setCombustivel(combustivel_key = this.state.combustivelSelecionado) {
    await this.setState(
      { combustivelSelecionado: combustivel_key },
      async () => {
        combustiveis = await this.getCombustiveis(this.props.postos);
        this.setState({ combustiveis });
      }
    );
  }

  render() {
    const { combustiveis } = this.state;

    return (
      <ScrollView>
        <CombustivelSelectList setCombustivel={this.setCombustivel} />

        {combustiveis.length > 0 &&
          combustiveis.map(combustivel => (
            <ListItem
              key={combustivel._id}
              leftAvatar={{
                rounded: true,
                title: "teste"
              }}
              title={"R$ " + maskPreco(combustivel.preco)}
              subtitle={combustivel.posto.nome}
              onPress={() => this.props.handleClick(combustivel.posto.id)}
            />
          ))}
      </ScrollView>
    );
  }
}
