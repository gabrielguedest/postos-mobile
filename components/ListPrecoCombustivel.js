import React from "react";

import moment from "moment";

import { StyleSheet } from "react-native";

import { View, Text, ScrollView } from "react-native";

import { Icon } from "react-native-elements";

import { maskCombustivel, maskPreco } from "../utils/masks";

export default (ListPrecoCombustivel = props => {
  return (
    <ScrollView style={{ width: "100%", display: "flex" }}>
      <Text style={styles.title}>Histórico de Preços</Text>
      {props.combustivel.map(item => {
        return (
          <View key={item._id} style={styles.item}>
            <Text style={styles.preco}>
              <Text style={{ fontSize: 14 }}>R$</Text> {maskPreco(item.preco)}
            </Text>
            <Text style={styles.data}>
              {"  "}-{"  " + moment(item.data).format("DD/MM/YYYY")}
            </Text>
          </View>
        );
      })}
    </ScrollView>
  );
});

const styles = StyleSheet.create({
  item: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 15
  },
  title: {
    textAlign: "center",
    fontSize: 22,
    fontWeight: "bold",
    paddingTop: 15,
    paddingBottom: 10
  },
  preco: {
    fontSize: 18
  },
  data: {
    fontSize: 14,
    color: "#8e8e8e"
  }
});
