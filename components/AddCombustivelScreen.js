import React, { Component } from "react"
import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  Alert,
  Image,
} from "react-native"
import { Header } from "react-native-elements"
import CombustivelSelectList from "./CombustivelSelectList"
import ApiHelper from '../src/ApiHelper'

export default class AddCombustivelScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      combustivelSelecionado: "gasolina",
      precoInput: "0,000",
      precoInvalido: false,
      loading: false,
      posicaoPreco: 4,
    };

    this.setCombustivel = this.setCombustivel.bind(this);
    this.cadastrarCombustivel = this.cadastrarCombustivel.bind(this);
  }

  static navigationOptions = {
    title: "Adicionar Combustível",
    header: null
  };

  async cadastrarCombustivel() {
    const postoId = this.props.navigation.getParam("id_posto")

    if (this.state.precoInput === '0,000') {
      this.setState({
        error: "Favor informar um preço válido!"
      });
    } else {
      const formattedPrice = parseFloat(this.state.precoInput.replace(',', '.'))

      const novoPreco = {
        combustivel: this.state.combustivelSelecionado,
        preco: formattedPrice,
      };

      this.setState({
        loading: true
      });

      try {
        const result = await ApiHelper.addCombustivel(novoPreco, postoId)

        if (result.status === 200) {
          this.setState({
            loading: false
          });
          Alert.alert(
            "Sucesso!",
            "Cadastro de combustível realizado com sucesso!",
            [{
              text: "OK",
              onPress: () =>
                this.props.navigation.navigate("Home", {
                  addCombustivel: true,
                  id_posto: postoId
                })
            }]
          );
        }
      } catch (error) {
        console.log(error.message)
      }
      
      this.setState({
        error: ""
      });
    }
  }

  setCombustivel(combustivelSelecionado) {
    this.setState({
      combustivelSelecionado
    });
  }

  // nao reparem na gambiarra que vou fazer aqui
  // recomendo nao reproduzir isso em casa

  handlePrice(button) {
    const {precoInput, posicaoPreco} = this.state

    if (!button) {
      return this.setState({ precoInput: '0,000', posicaoPreco: 4 })
    }

    if (posicaoPreco >= 0) {
      let preco

      if (posicaoPreco > 2) {
        precoAtual = precoInput.substr(posicaoPreco)
        preco = precoInput.substr(precoInput, posicaoPreco - 1) + precoAtual + button
      } else {
        primeiroDigito = precoInput.substr(2, 1)
        restante = precoInput.substr(3)

        preco = `${primeiroDigito},${restante}${button}`
      }

      nextPosition = posicaoPreco === 2
        ? 0
        : posicaoPreco - 1

      this.setState({ precoInput: preco, posicaoPreco: nextPosition })    
    }
  }

  renderButtons() {
    const buttons = ['1', '2', '3', '4', '5', '6', '7', '8', '9', false, '0', 'backspace']

    return buttons.map(b => {
      if (b === 'backspace') {
        return (<TouchableHighlight 
                  onPress={() => this.handlePrice(false)}
                  underlayColor="none"
                  style={styles.backspaceButton}>
                  <Image
                    source={require("../assets/bckspc.png")}
                    style={{ width: 30, height: 30 }}
                  />
                </TouchableHighlight>)
      }
      return b
        ? (<TouchableHighlight
              underlayColor="#ccc"
              onPress={() => this.handlePrice(b)}
              style={styles.inputButton}>
              <Text style={styles.textButton}>{b}</Text>
            </TouchableHighlight>)
        : (<TouchableHighlight 
              underlayColor="none"
              style={{ marginTop: 20, width: 90, height: 90, visibility: 'hidden' }}>
              <Text style={styles.textButton}></Text>
            </TouchableHighlight>)
    })
  }

  render() {
    const { navigation } = this.props
    const id_posto = navigation.getParam("id_posto")

    return ( 
      <View style={{
        position: "absolute",
        bottom: 0,
        top: 0,
        left: 0,
        right: 0
      }}>
        <Header 
          backgroundColor="#30336b"
          leftComponent={{
            icon: "arrow-back",
            color: "#fff",
            underlayColor: "#30336b",
            onPress: () => this.props.navigation.goBack()
          }}
          centerComponent = {{
            text: "Cadastrar Combustível",
            style: {
              color: "#fff",
              fontSize: 18,
              fontWeight: "bold"
            }
          }} 
        />
        <View style={styles.container}>
          <Text style={styles.price}>
            <Text style={styles.currency}>R$</Text>
            <Text style={{ letterSpacing: 3 }}>{this.state.precoInput}</Text>
          </Text>
        </View>
        <View style={{ marginTop: 20, marginLeft: 120, marginRight: 120 }}>
            <CombustivelSelectList setCombustivel={this.setCombustivel} />
        </View>
        <View style={styles.inputContainer}>
          {this.renderButtons()}
        </View>
        <TouchableHighlight 
          underlayColor="#23254f"
          style={styles.inputSaveButton}
          onPress={this.cadastrarCombustivel} >
          <Image
              source={require("../assets/check_icon.png")}
              style={{ width: 25, height: 25 }}
          />
        </TouchableHighlight>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#30336b",
    paddingTop: 50,
    paddingBottom: 35,
    paddingLeft: 100,
    paddingRight: 100,
    marginTop: -5,
    maxHeight: 500
  },
  price: {
    marginLeft: -10,
    textAlign: 'center',
    fontSize: 64,
    color: 'white',
    fontWeight: "bold",
  },
  currency: {
    fontSize: 14,
    fontWeight: 'normal',
    paddingRight: 55,
  },
  inputContainer: {
    maxWidth: 340,
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
  },
  inputButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#ccc",
    height: 90,
    width: 90,
    borderRadius: 90,
    marginTop: 20,
  },
  backspaceButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: 90,
    width: 90,
    borderWidth: 1,
    borderColor: 'transparent',
    marginTop: 10,
    paddingTop: 20,
  },
  textButton: {
    fontSize: 32,
    fontWeight: '200',
    color: 'rgba(0, 0, 0, 0.8)'
  },
  inputSaveButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: '#30336b',
    height: 90,
    width: 90,
    borderRadius: 90,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 20,
  },
});


/* <CombustivelSelectList 
  setCombustivel={this.setCombustivel} /> 
<Text> Preço: </Text>
<TextInput 
  onChangeText={text => this.handleText(text)}
  keyboardType="numeric"
  value={this.state.precoInput}
/>

<Button 
  title="Cadastrar"
  loading={this.state.loading}
  buttonStyle={{
    backgroundColor: "rgba(92, 99,216, 1)",
    width: 300,
    height: 45,
    borderColor: "transparent",
    borderWidth: 0,
    borderRadius: 5
  }}
  onPress={() => this.cadastrarCombustivel()}
/>  */