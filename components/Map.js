import React, { Component, Image } from "react"
import { StyleSheet, Text} from "react-native"
import MapView, { Marker, Callout } from "react-native-maps"


export default class Map extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialPosition: {
        latitude: -23.9681629,
        longitude: -46.3568542,
        latitudeDelta: 0.0042,
        longitudeDelta: 0.0031
      }
    };
  }

  render() {
    return (
      <MapView
        initialRegion={this.state.initialPosition}
        style={styles.map}
        showsPointsOfInterest={false}
        showsBuildings={false}
        showsIndoors={false}
        showsCompass={false}
        showsUserLocation={true}
        onPress={() => this.props.closeDetails()}
      >
        {this.props.postos.map(posto => (
          <Marker
            key={posto._id}
            coordinate={{
              latitude: posto.localizacao.lat,
              longitude: posto.localizacao.lng
            }}
            onPress={() => this.props.handleClick(posto)} 
            image={require('../assets/marker.png')} >
            <Callout>
              <Text style={{ padding: 10, fontFamily: "Roboto" }}>
                Bandeira {posto.bandeira}
              </Text>
            </Callout>
          </Marker>
        ))}
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    position: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: -100
  }
});
