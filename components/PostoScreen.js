import React, { Component } from "react"
import { View, StyleSheet, Text } from "react-native"
import { Header } from "react-native-elements"
import CombustivelSelect from "./CombustivelSelect"
import ListPrecoCombustivel from "./ListPrecoCombustivel"
import ApiHelper from "../src/ApiHelper"

export default class PostoScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      posto: {},
      combustivelSelecionado: []
    };

    this.setCombustivel = this.setCombustivel.bind(this);
  }

  static navigationOptions = {
    title: "Posto Detail",
    header: null
  };

  async componentDidMount() {
    const posto_id = this.props.navigation.getParam("posto_id")

    try{
      const result = await ApiHelper.getPostoById(posto_id)

      if (result) {
        this.setState({ 
          posto: result, 
          loading: false
        })
      }
    } catch (error) {
      console.log(error)
    }
  }

  async setCombustivel(combustivel_key) {
    await this.setState({ combustivelSelecionado: combustivel_key });
  }

  render() {
    const { combustivelSelecionado, posto } = this.state;

    return (
      <View
        style={{
          position: "absolute",
          bottom: 0,
          top: 0,
          left: 0,
          right: 0
        }}
      >
        <Header
          outerContainerStyles={{ borderBottomWidth: 0 }}
          backgroundColor="#30336b"
          leftComponent={{
            icon: "arrow-back",
            color: "#fff",
            underlayColor: "#30336b",
            onPress: () => this.props.navigation.goBack()
          }}
          rightComponent={{
            icon: "add",
            color: "#fff",
            underlayColor: "#30336b",
            onPress: () =>
              this.props.navigation.push("AddCombustivel", {
                id_posto: posto._id
              })
          }}
        />

        <View style={styles.container}>
          {!this.state.loading && (
            <View>
              <Text
                style={{
                  color: "#fff",
                  fontSize: 28,
                  fontWeight: "bold",
                  marginBottom: 10
                }}
              >
                {posto.nome}
              </Text>
              <Text style={{ color: "#fff", fontSize: 18, fontWeight: "200" }}>
                {posto.localizacao.endereco}, {posto.localizacao.numero}
              </Text>
              <Text style={{ color: "#fff", fontSize: 18, marginBottom: 10 }}>
                {posto.localizacao.bairro} - {posto.localizacao.cidade}
              </Text>
              <Text style={{ color: "#fff", fontSize: 16 }}>
                Bandeira {posto.bandeira}
              </Text>
              <Text
                style={{
                  color: "#fff",
                  fontSize: 64,
                  textAlign: "center",
                  marginTop: 15,
                  marginBottom: 15
                }}
              >
                {combustivelSelecionado.length > 0
                  ? (<Text>
                      <Text style={styles.currency}>R$</Text>
                      <Text>{`${combustivelSelecionado[0].preco}`.replace('.', ',')}</Text>
                    </Text>)
                  : "-"}
              </Text>
              <CombustivelSelect
                style={{ marginLeft: 100, marginRight: 100 }}
                combustiveis={posto.combustiveis}
                selectComb={this.setCombustivel}
              />
            </View>
          )}
        </View>
        <ListPrecoCombustivel combustivel={combustivelSelecionado} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#30336b",
    paddingTop: 20,
    paddingBottom: 25,
    paddingLeft: 20,
    paddingRight: 20,
    maxHeight: 500
  },
  currency: {
    fontSize: 14,
    fontWeight: 'normal',
    paddingRight: 55,
  },
})
