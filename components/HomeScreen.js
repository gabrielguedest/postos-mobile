import React, { Component } from "react"
import { StyleSheet, View, StatusBar, Text, Image } from "react-native"
import Navbar from "./Navbar"
import Map from "./Map"
import CombustivelDetail from "./CombustivelDetail"
import ApiHelper from '../src/ApiHelper'

export default class HomeScreen extends Component {
  static navigationOptions = {
    title: "PostosAPP",
    header: null
  }

  constructor() {
    super()

    this.state = {
      detailsVisible: false,
      postoSelecionado: {
        _id: ""
      },
      postos: [],
      error: null
    }

    this.markerClick = this.markerClick.bind(this)
    this.closeDetails = this.closeDetails.bind(this)
    this.navigatePostoScreen = this.navigatePostoScreen.bind(this)
    this.navigateListScreen = this.navigateListScreen.bind(this)
  }

  componentDidMount() {
    StatusBar.setHidden(true)
    this.loadPostos()
  }

  async loadPostos() {
    try {
      const postos = await ApiHelper.getPostos()
      if(postos) {
        this.setState({ postos })
      }
    } catch (error) {
      this.setState({ error: "Falha ao carregar os postos" })
      console.log(error)
    }
  }

  shouldComponentUpdate(nextProps) {
    // verificar quando as props forem diferentes do estado
    return true;
  }

  async componentDidUpdate() {
    await this.loadPostos()
  }

  async loadPostoById(id) {
    try {
      return await ApiHelper.getPostoById(id)
    } catch (error) {
      console.log(error)
    }
  }

  markerClick(posto) {
    const {postoSelecionado, detailsVisible} = this.state

    if (postoSelecionado._id !== "") {
      if (posto._id !== postoSelecionado._id)
        this.setState({
          detailsVisible: true,
          postoSelecionado: posto
        });
    } else {
      this.setState({
        detailsVisible: !detailsVisible,
        postoSelecionado: posto
      });
    }
  }

  closeDetails() {
    if (this.state.detailsVisible) {
      this.setState({
        detailsVisible: false,
        postoSelecionado: {
          _id: ""
        }
      })
    }
  }

  navigateListScreen() {
    this.props.navigation.push("List", { postos: this.state.postos });
  }

  navigatePostoScreen(posto_id) {
    this.props.navigation.push("Posto", { posto_id });
  }

  render() {
    const { postos } = this.state;

    return (
      <View style={styles.container}>
        <Navbar navigateListScreen={() => this.navigateListScreen} />
        {postos.length > 0 ? (
          <Map
            postos={postos}
            handleClick={this.markerClick}
            closeDetails={this.closeDetails}
          />
        ) : (
          <View style={styles.loading}>
            <Image 
              style={{ marginLeft: 'auto', marginRight: 'auto', width: 80, height: 80 }}
              source={require('../assets/spinner.gif')}
            />
            <Text style={{color: '#30336b'}}>Carregando postos</Text>
          </View>
        )}
        {this.state.detailsVisible && (
          <CombustivelDetail
            postoSelecionado={this.state.postoSelecionado}
            navigatePostoScreen={this.navigatePostoScreen}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  loading: {
    display: 'flex',
    justifyContent: 'center', 
    marginLeft: 'auto', 
    marginRight: 'auto'
  }
});
