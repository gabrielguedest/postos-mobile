import React, { Component } from "react";

import { View, TextInput } from "react-native";

import ModalSelector from "react-native-modal-selector";

export default class CombustivelSelectList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      opcoes: [
        {
          key: 1,
          label: "Gasolina",
          combustivel: "gasolina"
        },
        {
          key: 2,
          label: "Gasolina Aditivada",
          combustivel: "gasolina_aditivada"
        },
        {
          key: 3,
          label: "Gasolina Formulada",
          combustivel: "gasolina_formulada"
        },
        {
          key: 4,
          label: "Gasolina Premium",
          combustivel: "gasolina_premium"
        },
        {
          key: 5,
          label: "Etanol",
          combustivel: "etanol"
        },
        {
          key: 6,
          label: "Etanol Aditivado",
          combustivel: "etanol_aditivado"
        },
        {
          key: 7,
          label: "Diesel",
          combustivel: "diesel"
        },
        {
          key: 8,
          label: "GNV",
          combustivel: "gnv"
        }
      ],
      textInputValue: "Gasolina"
    };
  }

  render() {
    return (
      <ModalSelector
        data={this.state.opcoes}
        initValue="Selecione um combustível"
        cancelText="Cancelar"
        onChange={option => {
          this.setState({ textInputValue: option.label });
          this.props.setCombustivel(option.combustivel);
        }}
      >
        <TextInput
          style={{
            margin: 10,
            borderWidth: 1,
            borderColor: "#30336b",
            padding: 10,
            color: "#30336b",
            textAlign: "center"
          }}
          editable={false}
          placeholderTextColor="#30336b"
          value={this.state.textInputValue}
        />
      </ModalSelector>
    );
  }
}
